const express = require('express')
const bodyParser = require('body-parser')
const {GraphQLSchema} = require('graphql')
const graphqlHTTP = require('express-graphql').graphqlHTTP;
const app = express()
const dbPool = require('./db/mysql')
const RootQuery = require('./graphql/query')
const MutationType = require('./graphql/mutation')
const { faker } = require('@faker-js/faker');


const PORT = 3000
app.use(bodyParser.json())

const schema = new GraphQLSchema({ query: RootQuery, mutation: MutationType})

app.use('/graphql', graphqlHTTP({
    schema: schema,
    graphiql: true
}));



dbPool.connect(function(err) {
    if (err) throw err;
    console.log("DB Connected!");
  });

app.listen(PORT, () => {
    console.log("App started....")
})