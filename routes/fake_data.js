app.post('/fake_data', (req, res) => {
    function createRandomUser(){
       return {
         username: faker.internet.userName(),
         email: faker.internet.email(),
         avatar: faker.image.avatar(),
         password: faker.internet.password(),
         birthdate: faker.date.birthdate().getDate(),
         registeredAt: faker.date.past(),
       };
     }
     
     const users = faker.helpers.multiple(createRandomUser, {
       count: 5,
     });
   //   console.log(users)

     let query = "INSERT INTO users(id, username, email, password, avatar, birthdate, registration_date) VALUES";
     let dynamic_query=''

     users.forEach(user => {
       dynamic_query += `(0, "${user.username}", "${user.email}", "${user.password}", "${user.avatar}", null, NOW()),`
     })
     dynamic_query = dynamic_query.slice(0, -1)
     query += dynamic_query
     console.log(query)
     dbPool.query(query, (err, result) => {
       if (err) {
           console.log(err);
           return;
       }
    
       console.log(result)
     })
     res.status(200).send("OK")
});