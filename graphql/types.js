const {  GraphQLObjectType,  GraphQLList, GraphQLString,GraphQLInt} = require('graphql')

const UserType = new GraphQLObjectType({
    name: "user",
    fields: ()=>({
        id : { type: GraphQLInt },
        username : { type: GraphQLString },
        email : { type:  GraphQLString },
        password : { type: GraphQLString },
        birthdate : { type: GraphQLString },
        avatar : { type: GraphQLString },
     })

})

module.exports=UserType