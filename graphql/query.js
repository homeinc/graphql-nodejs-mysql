const UserType = require('./types')
const {  GraphQLObjectType,  GraphQLList, GraphQLInt} = require('graphql')
const dbPool = require('../db/mysql')

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
       getAllUsers: {
           type: new GraphQLList(UserType),
           resolve(parent, args) {
               return new Promise( (resolv, reject) => {
                    dbPool.query("SELECT * FROM users", (err, results) => {
                        if (err)
                            reject(err)
                        console.log(results)
                        resolv(results)
                    })
               })
           }
       },
       getUserById: {
            type : UserType,
            args: {
                     id : { type: GraphQLInt }
                },
             resolve(parent, args) {
                const { id} = args 
                return new Promise( (resolv, reject) => {
                    dbPool.query(`SELECT * FROM users WHERE id=${id}`, (err, result) => {
                        if (err)
                             reject(err)
                        console.log(result)
                    resolv(result[0])
                })
            })
        }
       }
    }
})

module.exports=RootQuery 