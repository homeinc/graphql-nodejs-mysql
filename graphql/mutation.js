const UserType = require('./types')
const {  GraphQLObjectType,  GraphQLList, GraphQLString,GraphQLInt} = require('graphql')
const dbPool = require ('../db/mysql')
const bcrypt = require('bcrypt')

const MutationType = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        createUser: {
            type: UserType,
            args: {
                username : { type: GraphQLString },
                email : { type:  GraphQLString },
                password : { type: GraphQLString },
                avatar : { type: GraphQLString },
                birthdate : { type: GraphQLString },
            },

            resolve(parent, args) {
                const { username, email, password, avatar, birthdate } = args
                return new Promise( (resolve, reject) => {
                    bcrypt.hash(password, 12, function(err, hash) {
                        if (err)
                            reject(err)
                        dbPool.query(`INSERT INTO users(username, email, password, avatar, birthdate) 
                                      values("${username}", "${email}", "${hash}", "${avatar}", "${birthdate}")`, (err, result) => {
                        if (err)
                           reject(err)
                        resolve(result)             
                        })
                    });
             })
        }
        },
    }
  });

  module.exports=MutationType